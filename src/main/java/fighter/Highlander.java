package fighter;

/**
 * Created by root on 01/10/16.
 */
public class Highlander extends Fighter{

    public Highlander(){
        super(150,"GreatSword",null);
    }

    public Highlander equip(String equipment){
        super.equip(equipment);
        return this;
    }

}
