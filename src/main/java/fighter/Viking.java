package fighter;

public class Viking extends Fighter{

    public Viking(){
        super(120,"Axe",null);
    }

    public Viking equip(String equipment){
        super.equip(equipment);
        return this;
    }

}
