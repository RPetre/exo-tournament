package fighter;


import equipment.*;

public abstract class Fighter {

    protected int hp;
    protected Weapon weapon;
    protected Buckler buckler;
    protected Armor armor;

    public Fighter(int hp, String weapon, Buckler buckler) {
        this.hp = hp;
        this.buckler = buckler;
        this.armor = null;
        switch (weapon){
            case "Sword":
                this.weapon = new Sword();
                break;
            case "Axe":
                this.weapon = new Axe();
                break;
            case "GreatSword":
                this.weapon = new GreatSword();
                break;
            default:
                break;
        }
    }

    public int getDamages() {
        return weapon.getDamages();
    }

    public void setHp(int damagesReceived){
        this.hp = this.hp - damagesReceived;
        if (this.hp < 0) {
            this.hp = 0;
        }
    }

    public int hitPoints(){
        return hp;
    }

    public Fighter equip(String weapon){
        switch (weapon) {
            case "buckler" :
                this.buckler = new Buckler();
                break;
            case "armor":
                this.armor = new Armor();
                break;
            default :
                break;
        }

        return this;
    }

    public Buckler testBuckler(){

        if (buckler.getBlowBlocked() != 0){
            buckler.setBlowBlocked();
        }
        else{
            buckler = null;
        }

        return buckler;
    }

    public void engage(Fighter opponent){

        int nbAttacks = 1;

        while (this.hitPoints() > 0  && opponent.hitPoints() > 0){

            this.hit(opponent,nbAttacks);
            opponent.hit(this,nbAttacks);
            nbAttacks ++;
        }
    }

    public void hit(Fighter opponent, int nbAttacks){
        this.weapon.setCanAttack(nbAttacks);
        if (this.weapon.getDamages() != 0){
            if (opponent.buckler != null){
                if (this.weapon.isBucklerBreaker()) {
                    if (nbAttacks % 2 != 0){
                        opponent.buckler = opponent.testBuckler();
                    }
                    else {
                        opponent.setHp(this.getDamages() - this.damageSendReduced() - opponent.damageReceiveReduced());
                    }
                }
                else {
                    if (nbAttacks % 2 != 0 ){
                        opponent.setHp(this.getDamages() - this.damageSendReduced() - opponent.damageReceiveReduced());
                    }
                }
            }
            else {
                opponent.setHp(this.getDamages() - this.damageSendReduced() - opponent.damageReceiveReduced());
            }
        }
        System.out.println("hp : " + this.hitPoints() + " damages : " + this.getDamages());

    }

    public int damageSendReduced(){
        if (this.armor != null){
            return armor.damageSendReduced();
        }
        else{
            return 0;
        }
    }

    public int damageReceiveReduced(){
        if (this.armor != null){
            return armor.damageReceiveReduced();
        }
        else {
            return 0;
        }
    }

}
