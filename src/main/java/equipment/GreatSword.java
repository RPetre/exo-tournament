package equipment;

public class GreatSword extends Weapon{

    public GreatSword(){
        super(12,false);
        canAttack = true;
    }

    public int getDamages(){
        if (canAttack){
            return this.damages;
        }
        else{
            return 0;
        }
    }

    public void setCanAttack(int nbAttacks) {
        this.canAttack = !(nbAttacks % 3 == 0);
    }
}
