package equipment;

public abstract class Weapon {
    protected int damages;
    protected boolean bucklerBreaker;
    protected boolean canAttack;

    public Weapon(int damages, boolean bucklerBreaker){
        this.damages = damages;
        this.bucklerBreaker = bucklerBreaker;
    }

    public int getDamages() {
        return damages;
    }

    public boolean isBucklerBreaker() {
        return bucklerBreaker;
    }

    public void setCanAttack(int nbAttacks) {
        this.canAttack = true;
    }
}
