package equipment;

/**
 * Created by root on 03/10/16.
 */
public class Buckler {
    private int blowBlocked;
    private boolean canBlock;

    public Buckler() {
        this.blowBlocked = 3;
        canBlock = true;
    }

    public int getBlowBlocked() {
        return blowBlocked;
    }

    public void setBlowBlocked() {
        this.blowBlocked -= blowBlocked;
    }

    public boolean isCanBlock() {
        return canBlock;
    }
}
